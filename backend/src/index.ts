import express from "express";
import dotenv from "dotenv";

import connectDB from "./config/db";
import setupSwagger from "./config/swagger";

import authRoutes from "./routes/auth";
import urlRoutes from "./routes/urlManagement";
import urlRedirect from "./routes/urlRedirect";

dotenv.config();

const server = express();

setupSwagger(server);

server.use(express.json());

server.use("/api/auth", authRoutes);
server.use("/api/urls", urlRoutes);
server.use("/u", urlRedirect);

const PORT = process.env.PORT || 5000;

connectDB().then(() => {
  server.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
  });
});
