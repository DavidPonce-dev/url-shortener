import mongoose, { Document, Schema } from "mongoose";
import { IUser } from "./User";

export type AccessType = "public" | "emailRestricted" | "private";

export interface IUrl extends Document {
  originalUrl: string;
  shortUrl: string;
  userId: IUser["_id"];
  clicks: number;
  category: string;
  accessType: AccessType;
  authorizedEmails?: string[];
  createdAt: Date;
}

const UrlSchema = new Schema<IUrl>({
  originalUrl: { type: String, required: true },
  shortUrl: { type: String, required: true, unique: true },
  userId: { type: Schema.Types.ObjectId, ref: "User", required: true },
  createdAt: { type: Date, default: Date.now },
  clicks: { type: Number, default: 0 },
  accessType: {
    type: String,
    enum: ["public", "emailRestricted", "private"],
    default: "public",
  },
  authorizedEmails: { type: [String], default: [] },
  category: { type: String, required: true },
});

export default mongoose.model<IUrl>("Url", UrlSchema);
