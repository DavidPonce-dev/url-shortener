import express from "express";
import {
  createShortUrl,
  getUserUrls,
  deleteUrl,
  updateUrl,
} from "../controllers/urlController";
import { protect } from "../middlewares/auth";

const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: URL Management
 *   description: Endpoints for managing shortened URLs
 */

/**
 * @swagger
 * /api/urls:
 *   get:
 *     summary: Retrieve all URLs for the authenticated user
 *     tags: [URL Management]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Successfully retrieved list of shortened URLs
 *       401:
 *         description: Unauthorized, user not authenticated
 *       500:
 *         description: Internal server error
 */
router.get("/", protect, getUserUrls);

/**
 * @swagger
 * /api/urls:
 *   post:
 *     summary: Create a new shortened URL
 *     tags: [URL Management]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               originalUrl:
 *                 type: string
 *                 format: uri
 *                 description: The original URL to be shortened
 *               accessType:
 *                 type: string
 *                 enum: [public, emailRestricted, private]
 *                 description: The access type for the shortened URL
 *               authorizedEmails:
 *                 type: array
 *                 items:
 *                   type: string
 *                   format: email
 *                 description: List of allowed emails for access if accessType is "emailRestricted"
 *               category:
 *                 type: string
 *                 description: The category of the shortened URL
 *     responses:
 *       201:
 *         description: Successfully created the shortened URL
 *       400:
 *         description: Invalid input, bad request
 *       500:
 *         description: Internal server error
 */
router.post("/", protect, createShortUrl);

/**
 * @swagger
 * /api/urls/{id}:
 *   put:
 *     summary: Update an existing shortened URL
 *     tags: [URL Management]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: The ID of the shortened URL to be updated
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               originalUrl:
 *                 type: string
 *                 format: uri
 *                 description: The updated original URL
 *               accessType:
 *                 type: string
 *                 enum: [public, emailRestricted, private]
 *                 description: The access type for the shortened URL
 *               authorizedEmails:
 *                 type: array
 *                 items:
 *                   type: string
 *                   format: email
 *                 description: Updated list of allowed emails if accessType is "emailRestricted"
 *               category:
 *                 type: string
 *                 description: The updated category of the shortened URL
 *     responses:
 *       200:
 *         description: Successfully updated the URL
 *       404:
 *         description: URL not found
 *       403:
 *         description: Forbidden, not authorized to edit the URL
 *       500:
 *         description: Internal server error
 */
router.put("/:id", protect, updateUrl);

/**
 * @swagger
 * /api/urls/{id}:
 *   delete:
 *     summary: Delete a shortened URL
 *     tags: [URL Management]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: The ID of the shortened URL to delete
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: Successfully deleted the URL
 *       404:
 *         description: URL not found
 *       401:
 *         description: Unauthorized, user not authenticated
 *       500:
 *         description: Internal server error
 */
router.delete("/:id", protect, deleteUrl);

export default router;
