import express from "express";
import { redirectToOriginalUrl } from "../controllers/urlController";

const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: URL Redirection
 *   description: Endpoints for URL redirection
 */

/**
 * @swagger
 * /u/{shortUrl}:
 *   get:
 *     summary: Redirect to the original URL
 *     tags: [URL Redirection]
 *     parameters:
 *       - in: path
 *         name: shortUrl
 *         required: true
 *         description: The shortened URL code
 *         schema:
 *           type: string
 *     responses:
 *       302:
 *         description: Redirects to the original URL
 *       404:
 *         description: Shortened URL not found
 *       500:
 *         description: Server error
 */

router.get("/:shortUrl", redirectToOriginalUrl);

export default router;
