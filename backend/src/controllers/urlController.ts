import { Request, Response } from "express";
import Url from "../models/Url";
import { nanoid } from "nanoid";
import { IUser } from "../models/User";

interface AuthRequest extends Request {
  user?: IUser;
}

export const createShortUrl = async (req: AuthRequest, res: Response) => {
  const { originalUrl, accessType, authorizedEmails, category } = req.body;

  try {
    const shortUrl = nanoid(8);
    const url = new Url({
      originalUrl,
      accessType,
      shortUrl,
      authorizedEmails,
      category,
      userId: req.user?._id,
    });

    await url.save();

    res.status(201).json(url);
  } catch (error) {
    res.status(500).json({ message: "Server error", error });
  }
};

export const getUserUrls = async (req: AuthRequest, res: Response) => {
  const userId = req.user?._id;

  try {
    const urls = await Url.find({ userId });
    res.json(urls);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

export const deleteUrl = async (req: AuthRequest, res: Response) => {
  const urlId = req.params.id;
  const userId = req.user?._id;

  try {
    const url = await Url.findOneAndDelete({ _id: urlId, userId });
    if (!url) return res.status(404).json({ message: "URL not found" });
    res.json({ message: "URL deleted successfully" });
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

export const updateUrl = async (req: AuthRequest, res: Response) => {
  const { originalUrl, access, emailAccess, category } = req.body;
  const { id } = req.params;

  try {
    const url = await Url.findById(id);
    if (!url) {
      return res.status(404).json({ message: "URL not found" });
    }

    if (
      (url.userId as IUser).toString() !== (req.user?._id as IUser).toString()
    ) {
      return res
        .status(403)
        .json({ message: "Not authorized to edit this URL" });
    }

    url.originalUrl = originalUrl || url.originalUrl;
    url.accessType = access || url.accessType;
    url.category = category || url.category;
    url.authorizedEmails = emailAccess || url.authorizedEmails;

    await url.save();
    res.json({ message: "URL updated successfully", url });
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

export const redirectToOriginalUrl = async (req: Request, res: Response) => {
  const { shortUrl } = req.params;
  const userEmail = req.query.email as string;

  try {
    const url = await Url.findOne({ shortUrl });
    if (!url) {
      return res.status(404).json({ message: "URL not found" });
    }
    if (url.accessType === "private") {
      return res
        .status(403)
        .json({ message: "Access to this URL is restricted" });
    }

    if (url.accessType === "emailRestricted") {
      if (!userEmail || !url.authorizedEmails?.includes(userEmail)) {
        return res
          .status(403)
          .json({ message: "Access denied: unauthorized email" });
      }
    }
    url.clicks += 1;
    await url.save();

    return res.redirect(url.originalUrl);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};
