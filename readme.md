# URL Shortener - Backend

Este repositorio contiene el backend de un acortador de URLs que permite a los usuarios gestionar enlaces con funcionalidades avanzadas. La aplicación incluye autenticación, control de acceso, categorización de enlaces, y un contador de clics.

## Estructura del Proyecto

La estructura de carpetas del proyecto es la siguiente:

```plaintext
url-shortener
|- backend
| |- src
|   |- config
|     |- db.ts
|     |- swagger.ts
| 
|   |- controllers
|     |- authController.ts
|     |- urlController.ts
|
|   |- middlewares
|     |- auth.ts
| 
|   |- models
|     |- Url.ts
|     |- User.ts
| 
|   |- routes
|     |- auth.ts
|     |- urlManagement.ts
|     |- urlRedirect.ts
|
|   |- utils
|     |- generateToken.ts
|
|   |- index.ts
|
| |- .gitignore
| |- package.json
| |- package-lock.json
| |- readme.md

|- frontend
| |- (Pendiente de implementación)
| |- ...
|- .gitignore
|- package.json
|- package-lock.json
|- readme.md
```

## Características Principales

- **Acortamiento de URLs**: Los usuarios pueden crear URLs acortadas que redirigen a una URL original.
- **Gestión de usuarios**: Autenticación y autorización mediante tokens JWT.
- **Categorías de URLs**: Clasificación de las URLs según una categoría definida por el usuario.
- **Control de acceso**: Las URLs pueden ser públicas, privadas o restringidas a correos electrónicos específicos.
- **Contador de clics**: Rastreo de cuántas veces se accede a cada URL.
- **Documentación con Swagger**: Documentación completa de la API generada automáticamente.

## Endpoints Principales

### CRUD de URLs

1. **Crear URL** (`POST /api/urls`)

   - Crea una URL acortada.
   - Requiere autenticación.

2. **Obtener URLs del usuario** (`GET /api/urls`)

   - Devuelve todas las URLs del usuario autenticado.
   - Requiere autenticación.

3. **Actualizar URL** (`PUT /api/urls/:id`)

   - Permite actualizar la URL original, el tipo de acceso, la lista de correos autorizados y la categoría.
   - Requiere autenticación y permisos sobre la URL.

4. **Eliminar URL** (`DELETE /api/urls/:id`)
   - Elimina una URL acortada.
   - Requiere autenticación y permisos sobre la URL.

## Instalación

1. Clona el repositorio:

   ```bash
   git clone https://github.com/tuusuario/url-shortener.git
   cd url-shortener/backend
   ```

2. Instala las dependencias:

   ```bash
   Copiar código
   npm install
   ```

3. Configura las variables de entorno en un archivo .env dentro de la carpeta de backend:

   ```plaintext
   MONGO_URI=tu_conexion_mongo
   JWT_SECRET=tu_secreto_jwt
   PORT=puerto_a_usar
   ```

4. Inicia el servidor:
   ```bash
   npm install
   ```

## Documentación de la API

Puedes acceder a la documentación generada por Swagger en:

```bash
http://localhost:{port}/api-docs
```

## Tecnologías Utilizadas en backend

Node.js y Express para el servidor.
MongoDB y Mongoose para la base de datos.
TypeScript para un tipado estático.
Swagger para la documentación de la API.

## Notas

Este backend se complementa con un frontend (pendiente de desarrollo) que permitirá a los usuarios interactuar con el sistema de acortamiento de URLs de manera intuitiva.
